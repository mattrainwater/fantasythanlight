﻿using FantasyThanLight.Battle.Effects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FantasyThanLight.Battle
{
    public class StatusEffect
    {
        public int Duration { get; set; }
        public Effect Effect { get; set; }
        public string Name { get; set; }
    }
}
