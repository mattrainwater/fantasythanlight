﻿using FantasyThanLight.Battle.Effects;
using FantasyThanLight.Common;
using FantasyThanLight.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FantasyThanLight.Battle
{
    public class Action
    {
        public TargetingType Targeting { get; set; }
        public List<Effect> Effects { get; set; }

        public void Apply(BattleController controller, BattleActor source)
        {
            var targets = GetTargets(controller, source);
            foreach(var effect in Effects)
            {
                effect.Apply(source, targets);
            }
        }

        public List<BattleActor> GetTargets(BattleController controller, BattleActor source)
        {
            var targets = new List<BattleActor>();
            var enemyUnits = controller.Actors.Where(x => x.Faction != source.Faction && !x.IsDead);
            var alliedUnits = controller.Actors.Where(x => x.Faction == source.Faction && !x.IsDead);
            var rows = Enum.GetValues(typeof(Row)).Cast<Row>().ToList();
            if (enemyUnits.Any())
            {
                switch (Targeting)
                {
                    case TargetingType.SingleMelee:
                        var minDistance = enemyUnits.Min(x => GetDistanceMelee(source, x));
                        var closestTargets = enemyUnits.Where(x => GetDistanceMelee(source, x) == minDistance);
                        if(closestTargets.Any(x => x.Position.Row == source.Position.Row))
                        {
                            targets.Add(closestTargets.First(x => x.Position.Row == source.Position.Row));
                        }
                        else
                        {
                            targets.Add(closestTargets.OrderBy(x => x.Position.Row).First());
                        }
                        break;
                    case TargetingType.SingleRanged:
                        var minRangedDistance = enemyUnits.Min(x => GetDistanceRanged(source, x));
                        var closestRangedTargets = enemyUnits.Where(x => GetDistanceRanged(source, x) == minRangedDistance);
                        if (closestRangedTargets.Any(x => x.Position.Row == source.Position.Row))
                        {
                            targets.Add(closestRangedTargets.First(x => x.Position.Row == source.Position.Row));
                        }
                        else
                        {
                            targets.Add(closestRangedTargets.OrderBy(x => x.Position.Row).First());
                        }
                        break;
                    case TargetingType.Row:
                        // any row with units in it
                        var populatedRows = enemyUnits.Select(x => x.Position.Row).OrderBy(x => x).Distinct();
                        if(populatedRows.Any(x => x == source.Position.Row))
                        {
                            targets.AddRange(enemyUnits.Where(x => x.Position.Row == source.Position.Row));
                        }
                        else
                        {
                            var firstPopulatedRow = populatedRows.First();
                            targets.AddRange(enemyUnits.Where(x => x.Position.Row == firstPopulatedRow));
                        }
                        break;
                    case TargetingType.ColumnRanged:
                        // any column with units in it
                        var populatedColumns = enemyUnits.Select(x => x.Position.Column).OrderBy(x => x).Distinct();
                        var farthestColumn = populatedColumns.First();
                        targets.AddRange(enemyUnits.Where(x => x.Position.Column == farthestColumn));
                        break;
                    case TargetingType.ColumnMelee:
                        // cloest column with units in it
                        var populatedColumnForMelee = enemyUnits.Select(x => x.Position.Column).Distinct().OrderByDescending(x => x).First();
                        targets.AddRange(enemyUnits.Where(x => x.Position.Column == populatedColumnForMelee));
                        break;
                    case TargetingType.AllEnemies:
                        // all enemies
                        targets.AddRange(enemyUnits);
                        break;
                    case TargetingType.All:
                        // everyone
                        targets.AddRange(enemyUnits);
                        targets.AddRange(alliedUnits);
                        break;
                    case TargetingType.Self:
                        // units self
                        targets.Add(source);
                        break;
                    case TargetingType.LowHealthAlly:
                        // lowest health allied unit
                        var lowestHealthAlly = alliedUnits.OrderBy(x => (float)x.CurrentHP / x.MaxHP).FirstOrDefault();
                        targets.Add(lowestHealthAlly);
                        break;
                    case TargetingType.AllAllies:
                        // all allies
                        targets.AddRange(alliedUnits);
                        break;
                    default:
                        // nothing
                        break;
                }
            }
            return targets;
        }

        public string GetDescription()
        {
            return Targeting.ToString() + " - " + String.Join(" and ", Effects.Select(x => x.GetDescription()));
        }

        private int GetDistanceMelee(BattleActor source, BattleActor target)
        {
            return Math.Abs(source.Position.Row - target.Position.Row) + Math.Abs(target.Position.Column - Column.Vanguard);
        }

        private int GetDistanceRanged(BattleActor source, BattleActor target)
        {
            return Math.Abs(source.Position.Row - target.Position.Row) + Math.Abs((int)target.Position.Column);
        }
    }
}
