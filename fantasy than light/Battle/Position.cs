﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FantasyThanLight.Battle
{
    public class Position
    {
        public Position(Row row, Column col)
        {
            Row = row;
            Column = col;
        }

        public Column Column { get; set; }
        public Row Row { get; set; }

        public static bool operator ==(Position p1, Position p2)
        {
            if(ReferenceEquals(p1, null))
            {
                return ReferenceEquals(p2, null);
            }
            else if(ReferenceEquals(p2, null))
            {
                return ReferenceEquals(p1, null);
            }
            return p1.Row == p2.Row && p1.Column == p2.Column;
        }

        public static bool operator !=(Position p1, Position p2)
        {
            return !(p1 == p2);
        }
    }
}
