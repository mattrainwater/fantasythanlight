﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FantasyThanLight.Battle
{
    public class Ability
    {
        public AbilityType Type { get; set; }
        public int Value { get; set; }
    }
}
