﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FantasyThanLight.Battle
{
    public enum StunStatus
    {
        None,
        Recovering,
        Stunned
    }
}
