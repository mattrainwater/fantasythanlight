﻿using FantasyThanLight.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FantasyThanLight.Battle.Phases
{
    public abstract class Phase
    {
        public bool IsFinished { get; set; }
        public BattleController Controller { get; set; }

        public Phase(BattleController controller)
        {
            Controller = controller;
        }

        public abstract void Initialize(List<BattleActor> actors);
        public abstract void Update();
    }
}
