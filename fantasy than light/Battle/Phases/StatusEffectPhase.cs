﻿using FantasyThanLight.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FantasyThanLight.Battle.Phases
{
    public class StatusEffectPhase : Phase
    {
        public List<BattleActor> Actors { get; set; }

        public StatusEffectPhase(BattleController controller)
            : base(controller)
        {

        }

        public override void Initialize(List<BattleActor> actors)
        {
            IsFinished = false;
            Actors = actors.Where(x => !x.IsDead).ToList();
        }

        public override void Update()
        {
            foreach(var actor in Actors)
            {
                if(actor.StatusEffects.Any())
                {
                    foreach(var effect in actor.StatusEffects)
                    {
                        effect.Effect.Apply(actor, actor);
                        effect.Duration--;
                    }
                    actor.StatusEffects = actor.StatusEffects.Where(x => x.Duration != 0).ToList();
                }
            }
            IsFinished = true;
        }
    }
}
