﻿using FantasyThanLight.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FantasyThanLight.Battle.Phases
{
    public class CombatPhase : Phase
    {
        public List<BattleActor> ActorsLeftInRound { get; set; }
        public BattleActor CurrentActor { get; set; }

        public CombatPhase(BattleController controller)
            : base(controller)
        {

        }

        public override void Initialize(List<BattleActor> actors)
        {
            IsFinished = false;
            ActorsLeftInRound = GenerateNewRound(actors);
            CurrentActor = ActorsLeftInRound.First();
            ActorsLeftInRound.Remove(CurrentActor);
            CurrentActor.ExecuteAttack();
        }

        public override void Update()
        {
            // if current actor is done
            if (CurrentActor.IsFinished)
            {
                // if there are still actors left in the round
                if (ActorsLeftInRound.Any())
                {
                    // grab the next one and Execute attack
                    CurrentActor = ActorsLeftInRound.First();
                    ActorsLeftInRound.Remove(CurrentActor);
                    if(!CurrentActor.IsDead)
                    {
                        CurrentActor.ExecuteAttack();
                    }
                    else
                    {
                        CurrentActor.IsFinished = true;
                    }
                }
                else
                {
                    // we're done with this combat phase
                    IsFinished = true;
                }
            }
        }

        private List<BattleActor> GenerateNewRound(List<BattleActor> actors)
        {
            var roundListOfActors = new List<BattleActor>();
            var livingActors = actors.Where(x => !x.IsDead && x.Attacks.ContainsKey(x.Position.Column)).ToList();
            return livingActors
                .OrderByDescending(x => x.Attacks[x.Position.Column].Priority)
                .ThenBy(x => x.Faction)
                .ThenBy(x => x.Position.Row)
                .ThenBy(x => x.Position.Column)
                .ToList();
        }
    }
}
