﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FantasyThanLight.Components;
using FantasyThanLight.Common;
using Nez;
using Microsoft.Xna.Framework;
using FantasyThanLight.Common.IntervalTree;
using Nez.UI;
using Nez.Sprites;

namespace FantasyThanLight.Battle.Phases
{
    public class RepositionPhase : Phase
    {
        public List<BattleActor> Actors { get; set; }

        private BattleActor _selectedActor;
        private Window _window;

        public RepositionPhase(BattleController controller, Window window)
            : base(controller)
        {
            _window = window;
        }

        public override void Initialize(List<BattleActor> actors)
        {
            IsFinished = false;
            Actors = actors;
            foreach(var square in Controller.Squares.Where(x => x.Faction == Faction.Player))
            {
                square.OnLeftClick = SelectPlayer;
            }
            foreach (var square in Controller.Squares)
            {
                square.OnRightClick = ShowUnitWindow;
                square.OnHover = ShowTargets;
                square.OnBlur = Clear;
            }
        }

        public override void Update()
        {
            if (VirtualButtons.MouseLeft.isPressed && _window.isVisible())
            {
                _window.setIsVisible(false);
            }
            else if (VirtualButtons.SelectInput.isPressed)
            {
                foreach(var actor in Actors.Where(x => x.Faction == Faction.Player && (x.Stun == StunStatus.Stunned || x.Stun == StunStatus.Recovering)))
                {
                    actor.Stun = actor.Stun--;
                }
                IsFinished = true;
                _window.setIsVisible(false);
                foreach (var square in Controller.Squares)
                {
                    square.Reset();
                }
            }
        }

        private void Clear(BattleSquare square)
        {
            square.getComponent<Sprite>().enabled = false;
            if (square.Actor != null)
            {
                square.Actor.ClearTargets();
            }
        }

        private void ShowTargets(BattleSquare square)
        {
            square.getComponent<Sprite>().enabled = true;
            square.getComponent<Sprite>().setColor(new Color(75, 75, 75, 75));
            if (square.Actor != null)
            {
                square.Actor.ShowTargets();
            }
        }

        private void SelectPlayer(BattleSquare square)
        {
            if (!_window.isVisible())
            {
                if (_selectedActor != null)
                {
                    if (square.Actor != null)
                    {
                        square.Actor.Move(_selectedActor.Position);
                    }
                    _selectedActor.Move(square.Position);
                    _selectedActor = null;
                }
                else if (square.Actor != null)
                {
                    _selectedActor = square.Actor;
                }
            }
        }

        private void ShowUnitWindow(BattleSquare square)
        {
            _selectedActor = null;
            if(square.Actor != null)
            {
                _window.setIsVisible(true);
                LoadWindow(square.Actor);
            }
        }

        private void LoadWindow(BattleActor actorAtPos)
        {
            _window.clearChildren();
            _window.top().left();
            var nameTable = _window.add(new Table()).setExpandX().setFillX().top().left().getElement<Table>();
            _window.row().setPadTop(10);
            var leftTable = _window.add(new Table()).setExpandX().setFillX().top().left().getElement<Table>();
            var rightTable = _window.add(new Table()).setExpandX().setFillX().top().left().getElement<Table>();
            _window.row().setPadTop(10);

            AddTextToWindow(nameTable, "Name: " + actorAtPos.Character.Name);

            AddTextToWindow(leftTable, "Max HP: " + actorAtPos.Character.MaxHP.ToString());
            leftTable.row();
            AddTextToWindow(leftTable, "Armor: " + actorAtPos.Character.Armor.ToString());
            leftTable.row().setPadTop(10);
            AddTextToWindow(leftTable, "Attacks:");
            leftTable.row();
            AddTextToWindow(leftTable, "Vanguard: " + actorAtPos.Attacks[Column.Vanguard].Name);
            leftTable.row();
            AddTextToWindow(leftTable, actorAtPos.Attacks[Column.Vanguard].GetDescription());
            leftTable.row().setPadTop(10);
            AddTextToWindow(leftTable, "Flank: " + actorAtPos.Attacks[Column.Flank].Name);
            leftTable.row();
            AddTextToWindow(leftTable, actorAtPos.Attacks[Column.Flank].GetDescription());
            leftTable.row().setPadTop(10);
            AddTextToWindow(leftTable, "Rear: " + actorAtPos.Attacks[Column.Rear].Name);
            leftTable.row();
            AddTextToWindow(leftTable, actorAtPos.Attacks[Column.Rear].GetDescription());

            AddTextToWindow(rightTable, "Speed: " + actorAtPos.Character.Speed.ToString());
            rightTable.row();
            AddTextToWindow(rightTable, "Resistance: " + actorAtPos.Character.Resistance.ToString());
            rightTable.row();
            //AddTextToWindow(rightTable, "Abilities:");
            //rightTable.row();
            //AddTextToWindow(rightTable, "Vanguard: " + actorAtPos.Attacks[Column.Vanguard].Name);
            //rightTable.row();
            //AddTextToWindow(rightTable, "Flank: " + actorAtPos.Attacks[Column.Flank].Name);
            //rightTable.row();
            //AddTextToWindow(rightTable, "Rear: " + actorAtPos.Attacks[Column.Rear].Name);

            //_window.setDebug(true);
            //nameTable.setDebug(true);
            //leftTable.setDebug(true);
            //rightTable.setDebug(true);
        }

        private void AddTextToWindow(Table table, string text)
        {
            var label = new Label(text);
            label.setEllipsis(true);
            table.add(label).setExpandX().setFillX().setMinWidth(0);
        }
    }
}
