﻿namespace FantasyThanLight.Battle
{
    public enum TargetingType
    {
        SingleMelee,
        SingleRanged,
        ColumnMelee,
        Row,
        ColumnRanged,
        AllEnemies,
        All,
        Self,
        LowHealthAlly,
        AllAllies,
    }
}