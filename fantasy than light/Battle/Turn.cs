﻿using FantasyThanLight.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FantasyThanLight.Battle
{
    public class Turn
    {
        public bool IsFinished { get; set; }
        public CardBattleController Controller { get; set; }
        public Column Column { get; set; }
        public Player CurrentPlayer { get; set; }
        public Player OtherPlayer { get; set; }


        public void Initialize()
        {

        }

        public void Update()
        {

        }
    }
}
