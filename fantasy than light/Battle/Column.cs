﻿namespace FantasyThanLight.Battle
{
    public enum Column
    {
        Vanguard = 2,
        Flank = 1,
        Rear = 0
    }
}