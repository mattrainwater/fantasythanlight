﻿using FantasyThanLight.Components;
using Microsoft.Xna.Framework;
using Nez;
using System.Collections.Generic;
using System;

namespace FantasyThanLight.Battle.Effects
{
    public abstract class Effect
    {
        public void Apply(BattleActor source, BattleActor target)
        {
            Apply(source, new List<BattleActor> { target });
        }

        public abstract void Apply(BattleActor source, List<BattleActor> targets);

        protected void DisplayEffect(int number, Color color, BattleActor target)
        {
            var effectDisplayContainer = target.getComponent<EffectDisplayContainer>();
            effectDisplayContainer.MakeEffectDisplay(number.ToString(), color, BattleController.TURN_LENGTH);
        }

        public abstract string GetDescription();
    }
}