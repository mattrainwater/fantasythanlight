﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FantasyThanLight.Components;
using Microsoft.Xna.Framework;
using Nez;

namespace FantasyThanLight.Battle.Effects
{
    public class HealingEffect : Effect
    {
        public int MinHealing { get; set; }
        public int MaxHealing { get; set; }

        public override void Apply(BattleActor source, List<BattleActor> targets)
        {
            foreach (var target in targets)
            {
                var randomDamage = Random.range(MinHealing, MaxHealing);
                var calcValue = randomDamage;
                calcValue = calcValue < 0 ? 0 : calcValue;
                target.CurrentHP += calcValue;
                DisplayEffect(calcValue, Color.Green, target);
                if (target.CurrentHP > target.MaxHP)
                {
                    target.CurrentHP = target.MaxHP;
                }
            }
        }

        public override string GetDescription()
        {
            return $"Heals target {MinHealing} - {MaxHealing} HP";
        }
    }
}
