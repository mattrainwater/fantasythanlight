﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FantasyThanLight.Components;
using Microsoft.Xna.Framework;
using Nez;

namespace FantasyThanLight.Battle.Effects
{
    public class DamageEffect : Effect
    {
        public DamageType Type { get; set; }
        public int MinDamage { get; set; }
        public int MaxDamage { get; set; }

        protected int _totalDamageDone;

        public override void Apply(BattleActor source, List<BattleActor> targets)
        {
            _totalDamageDone = 0;
            foreach (var target in targets)
            {
                var randomDamage = (float)Random.range(MinDamage, MaxDamage);
                var calcValue = (int)(randomDamage - randomDamage * GetReduction(target));
                calcValue = calcValue < 0 ? 0 : calcValue;
                target.CurrentHP -= calcValue;
                _totalDamageDone += calcValue;
                DisplayEffect(calcValue, Color.Red, target);
                if (target.CurrentHP <= 0)
                {
                    target.CurrentHP = 0;
                    target.IsDead = true;
                    // trigger ondie event
                }
            }
        }

        private float GetReduction(BattleActor target)
        {
            var randomPercentage = Random.range(.08f, .12f);
            if (Type == DamageType.Physical)
            {
                return randomPercentage * (target.Armor + GetBuff(target, AbilityType.ArmorBuff));
            }
            else
            {
                return randomPercentage * (target.Resistance + GetBuff(target, AbilityType.ResistanceBuff));
            }
        }

        private float GetBuff(BattleActor target, AbilityType type)
        {
            if (target.Abilities.ContainsKey(target.Position.Column))
            {
                var abilities = target.Abilities[target.Position.Column];
                var buff = abilities.FirstOrDefault(x => x.Type == type);
                if (buff != null)
                {
                    return buff.Value;
                }
            }
            return 0;
        }

        public override string GetDescription()
        {
            return $"Deals target {MinDamage} - {MaxDamage} HP";
        }
    }
}
