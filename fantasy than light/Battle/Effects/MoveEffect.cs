﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FantasyThanLight.Components;
using Nez;

namespace FantasyThanLight.Battle.Effects
{
    public class MoveEffect : Effect
    {
        // positive for "pull", negative for "push" - column
        public int MoveX { get; set; }
        // positive for "up", negative for "down" - row
        public int MoveY { get; set; }

        public override void Apply(BattleActor source, List<BattleActor> targets)
        {
            var allOpposingActors = source.Controller.Actors.Where(x => x.Faction != source.Faction);
            // TODO need to not push/pull onto allies
            foreach (var target in targets)
            {
                var nextPosition = new Position(target.Position.Row, target.Position.Column);
                if(MoveX != 0)
                {
                    var col = target.Position.Column;
                    nextPosition.Column = (Column)Mathf.clamp((int)col + MoveX, 0, 2);
                    if(IsOccupied(allOpposingActors, target.Position.Row, nextPosition.Column))
                    {
                        nextPosition.Column = col;
                    }
                }
                if(MoveY != 0)
                {
                    var row = target.Position.Row;
                    nextPosition.Row = (Row)Mathf.clamp((int)row + MoveY, 0, 2);
                    if (IsOccupied(allOpposingActors, nextPosition.Row, target.Position.Column))
                    {
                        nextPosition.Row = row;
                    }
                }
                target.Move(nextPosition);
                if(target.Stun == StunStatus.None)
                {
                    target.Stun = StunStatus.Stunned;
                }
            }
        }

        public override string GetDescription()
        {
            return "Moves the target";
        }

        private bool IsOccupied(IEnumerable<BattleActor> allOpposingActors, Row row, Column column)
        {
            return allOpposingActors.Any(x => x.Position.Row == row && x.Position.Column == column);
        }
    }
}
