﻿using FantasyThanLight.Components;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FantasyThanLight.Battle.Effects
{
    public class LifedrainEffect : DamageEffect
    {
        public override void Apply(BattleActor source, List<BattleActor> targets)
        {
            base.Apply(source, targets);

            var calcValue = _totalDamageDone;
            calcValue = calcValue < 0 ? 0 : calcValue;
            source.CurrentHP += calcValue;
            DisplayEffect(calcValue, Color.Green, source);
            if (source.CurrentHP > source.MaxHP)
            {
                source.CurrentHP = source.MaxHP;
            }
        }

        public override string GetDescription()
        {
            return base.GetDescription() + " and heals the user";
        }
    }
}
