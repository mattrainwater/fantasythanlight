﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FantasyThanLight.Components;

namespace FantasyThanLight.Battle.Effects
{
    public class ApplyStatusEffect : Effect
    {
        public Effect StatusEffect { get; set; }
        public int Duration { get; set; }
        public string Name { get; set; }

        public override void Apply(BattleActor source, List<BattleActor> targets)
        {
            foreach (var target in targets)
            {
                // add to duration if already has same type of status
                var existingStatus = target.StatusEffects.FirstOrDefault(x => x.Name == Name);
                if(existingStatus == null)
                {
                    target.StatusEffects.Add(new StatusEffect
                    {
                        Effect = StatusEffect,
                        Duration = Duration,
                        Name = Name
                    });
                }
                else
                {
                    existingStatus.Duration = Duration;
                }
            }
        }

        public override string GetDescription()
        {
            return $"Applies status effect";
        }
    }
}
