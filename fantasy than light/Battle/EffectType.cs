﻿namespace FantasyThanLight.Battle
{
    public enum EffectType
    {
        Damage,
        Healing
    }
}