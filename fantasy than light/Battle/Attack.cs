﻿using FantasyThanLight.Common;
using FantasyThanLight.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FantasyThanLight.Battle
{
    public class Attack
    {
        public string Name { get; set; }
        public int Priority { get; set; }
        public List<Action> Actions { get; set; }
        // animation

        public void Apply(BattleController controller, BattleActor actor)
        {
            foreach(var action in Actions)
            {
                action.Apply(controller, actor);
            }
        }

        public List<BattleActor> GetTargets(BattleController controller, BattleActor source)
        {
            return Actions.SelectMany(x => x.GetTargets(controller, source)).ToList();
        }

        public string GetDescription()
        {
            return String.Join(" ", Actions.Select(x => x.GetDescription()));
        }
    }
}
