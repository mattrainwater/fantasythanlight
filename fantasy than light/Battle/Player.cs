﻿using FantasyThanLight.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FantasyThanLight.Battle
{
    public class Player
    {
        public Player(List<Card> deck, Faction faction)
        {
            Faction = faction;
            Deck = deck;
            Hand = new List<Card>();
            DiscardPile = new List<Card>();
            Actors = new List<CardActor>();
        }

        public List<Card> Deck { get; set; }
        public List<Card> Hand { get; set; }
        public List<Card> DiscardPile { get; set; }
        public Faction Faction { get; set; }
        public List<CardActor> Actors { get; set; }
    }
}
