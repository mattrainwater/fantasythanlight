﻿using FantasyThanLight.Common;
using Microsoft.Xna.Framework;
using Nez;
using Nez.Splines;
using Nez.Tweens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FantasyThanLight.Components
{
    public class MoveToPoint : Component, IUpdatable
    {
        private Vector2 _targetPosition;
        private float _timeToLast;
        private Action _onFinishMove;
        private float _fudgeFactor = .25f;

        public MoveToPoint(Vector2 targetPosition, float timeToLast, Action onFinishMove)
        {
            _targetPosition = targetPosition;
            _timeToLast = timeToLast;
            _onFinishMove = onFinishMove;
        }

        public override void onAddedToEntity()
        {
        }
  
        public void update()
        {
            if (entity.position != _targetPosition)
            {
                var previousPos = transform.position;
                var nextPosition = Lerps.lerp(entity.position, _targetPosition, .005f);
                if (Vector2.Distance(entity.position, nextPosition) <= _fudgeFactor)
                {
                    entity.position = _targetPosition;
                }
                else
                {
                    entity.position = nextPosition;
                }
            }
            else
            {
                _onFinishMove();
                entity.removeComponent(this);
            }
        }
    }
}
