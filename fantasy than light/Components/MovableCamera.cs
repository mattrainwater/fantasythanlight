﻿using FantasyThanLight.Common;
using Microsoft.Xna.Framework;
using Nez;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FantasyThanLight.Components
{
    public class MovableCamera : Component, IUpdatable
    {
        public void update()
        {
            var leftRight = 0;
            var upDown = 0;
            if (VirtualButtons.LeftInput.isDown)
            {
                leftRight = -1;
            }
            else if(VirtualButtons.RightInput.isDown)
            {
                leftRight = 1;
            }

            if (VirtualButtons.UpInput.isDown)
            {
                upDown = -1;
            }
            else if (VirtualButtons.DownInput.isDown)
            {
                upDown = 1;
            }

            entity.scene.camera.setPosition(entity.scene.camera.position + new Vector2(leftRight, upDown));
        }
    }
}
