﻿using FantasyThanLight.Common;
using FantasyThanLight.Scenes;
using Microsoft.Xna.Framework;
using Nez;
using Nez.Tiled;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FantasyThanLight.Components
{
    public class OverworldController : Component, IUpdatable
    {
        private readonly Dictionary<int, List<Point>> HEX_NEIGHBORS = new Dictionary<int, List<Point>> {
            {1, new List<Point> { new Point(0, -1), new Point(-1, 0), new Point(1, 0), new Point(1, -1), new Point(0, 1), new Point(-1, -1) } },
            {0, new List<Point> { new Point(0, -1), new Point(-1, 0), new Point(1, 0), new Point(-1, 1), new Point(0, 1), new Point(1, 1) } }
        };

        public OverworldParty Party { get; set; }
        public TiledMap Map { get; set; }
        public EventController EventController { get; set; }

        public OverworldController(OverworldParty party, EventController eventController, TiledMap map)
        {
            Map = map;
            Party = party;
            EventController = eventController;
        }

        public void update()
        {
            if (!Party.IsMoving && !EventController.IsOpen)
            {
                var mousePos = Input.mousePosition;
                var worldPos = entity.scene.camera.screenToWorldPoint(mousePos);
                worldPos = new Vector2(worldPos.X + 14, worldPos.Y - 24.5f);
                var tilePos = Map.hexagonalWorldToTilePosition(worldPos);
                if (VirtualButtons.MouseLeft.isPressed)
                {
                    if (ValidPos(tilePos) && !Party.IsMoving)
                    {
                        var tile = Map.getLayer<TiledHexagonalTileLayer>(0).getTile(tilePos.X, tilePos.Y);
                        var tileProps = Map.getTilesetTile(tile.id).properties;
                        Party.SetPosition(tilePos, EventController.FireEvent, tileProps);
                    }
                }
            }
        }

        private bool ValidPos(Point tilePos)
        {
            // fits in map
            var tileSet = Map.getLayer<TiledHexagonalTileLayer>(0);
            var isInMap = tilePos.X >= 0 && tilePos.X < tileSet.width && tilePos.Y >= 0 && tilePos.Y < tileSet.height;
            if (isInMap)
            {
                var tile = tileSet.getTile(tilePos.X, tilePos.Y);
                var tileProps = Map.getTilesetTile(tile.id).properties;

                // is in range
                var isEven = Party.Position.X % 2;
                var validNeighbors = HEX_NEIGHBORS[isEven];
                var isInRange = validNeighbors.Any(x => x + tilePos == Party.Position);

                // is not current pos
                var isNotCurrentPos = tilePos.X != Party.Position.X || tilePos.Y != Party.Position.Y;

                var isWalkable = tileProps["terrain"] != "shallow_ocean" && tileProps["terrain"] != "ocean" && tileProps["terrain"] != "icy_ocean";

                return isInRange && isNotCurrentPos && isWalkable;
            }
            return false;
        }
    }
}
