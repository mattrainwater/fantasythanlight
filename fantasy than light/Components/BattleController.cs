﻿using FantasyThanLight.Battle;
using FantasyThanLight.Battle.Phases;
using FantasyThanLight.Common;
using FantasyThanLight.Common.IntervalTree;
using FantasyThanLight.Scenes;
using Microsoft.Xna.Framework;
using Nez;
using Nez.Sprites;
using Nez.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FantasyThanLight.Components
{
    public class BattleController : Component, IUpdatable
    {
        public const int HORIZONTAL_OFFSET = 96;
        public const int VERTICAL_OFFSET = 64;
        public const int CHARACTER_GRID = 64;
        private Phase _currentPhase;

        public const float TURN_LENGTH = 1f;

        public bool Paused { get; set; }
        public Text AttackName { get; set; }
        public List<BattleActor> Actors { get; set; }
        public List<Phase> Phases { get; set; }
        public bool Initialized { get; set; }

        public List<BattleSquare> Squares { get; set; }

        public BattleController(Window window)
        {
            Actors = new List<BattleActor>();
            Phases = new List<Phase> { new RepositionPhase(this, window), new CombatPhase(this), new StatusEffectPhase(this) };
            Squares = new List<BattleSquare>();
        }

        public override void onAddedToEntity()
        {
            AttackName = entity.addComponent(new Text(CommonResources.DefaultBitmapFont, "", new Vector2(1.0f, 1.0f), Color.Black));
            var rows = Enum.GetValues(typeof(Row)).Cast<Row>();
            var columns = Enum.GetValues(typeof(Column)).Cast<Column>();
            foreach(var row in rows)
            {
                foreach(var column in columns)
                {
                    var playerSquareEntity = entity.scene.createEntity("square");
                    var enemySquareEntity = entity.scene.createEntity("square");

                    var playerX = HORIZONTAL_OFFSET + ((int)column * CHARACTER_GRID);
                    var enemyX = DimensionConstants.DESIGN_WIDTH - HORIZONTAL_OFFSET - (((int)column + 1) * CHARACTER_GRID);
                    var y = VERTICAL_OFFSET + ((int)row * CHARACTER_GRID);
                    Squares.Add(playerSquareEntity.addComponent(new BattleSquare(new Position(row, column), Faction.Player, playerX, y, CHARACTER_GRID, CHARACTER_GRID)));
                    Squares.Add(enemySquareEntity.addComponent(new BattleSquare(new Position(row, column), Faction.Enemy, enemyX, y, CHARACTER_GRID, CHARACTER_GRID)));
                }
            }
        }

        public void AddActor(BattleActor actor, Faction faction)
        {
            var effectDisplayContainer = actor.entity.addComponent(new EffectDisplayContainer());
            var hpBar = actor.entity.addComponent(new FillupBar(2, 32, Color.Red, Color.Pink));
            hpBar.localOffset = new Vector2(16, 16f);
            actor.EffectDisplayContainer = effectDisplayContainer;
            actor.HPBar = hpBar;
            actor.Faction = faction;
            actor.Controller = this;
            Actors.Add(actor);
            var sprite = actor.entity.getComponent<Sprite>();
            sprite.flipX = actor.Faction == Faction.Enemy;
            sprite.origin = Vector2.Zero;
            sprite.renderLayer = 5;
            actor.entity.setScale(2f);
        }

        public void update()
        {
            if(!Initialized)
            {
                Initialized = true;
                AssignActorsToSquares();
            }
            if(VirtualButtons.D0Input.isPressed)
            {
                Core.startSceneTransition(new FadeTransition(() => new TestBattleScene()));
            }
            if (!Paused)
            {
                if (_currentPhase == null || _currentPhase.IsFinished)
                {
                    _currentPhase = GetNextPhase(_currentPhase);
                    _currentPhase.Initialize(Actors);
                }
                _currentPhase.Update();
            }
        }

        private void AssignActorsToSquares()
        {
            foreach(var actor in Actors)
            {
                var square = Squares.First(x => x.Position == actor.Position && x.Faction == actor.Faction);
                actor.Move(square.Position);
            }
        }

        private Phase GetNextPhase(Phase currentPhase)
        {
            if (currentPhase == null)
            {
                return Phases.First();
            }
            else
            {
                var index = Phases.IndexOf(currentPhase);
                index = index == Phases.Count() - 1 ? 0 : index + 1;
                return Phases[index];
            }
        }
    }
}
