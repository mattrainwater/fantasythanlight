﻿using FantasyThanLight.Battle;
using FantasyThanLight.Common;
using Microsoft.Xna.Framework;
using Nez;
using Nez.Sprites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FantasyThanLight.Components
{
    public class BattleSquare : Component, IUpdatable
    {
        public Sprite Sprite { get; set; }
        public RectangleF Rectangle { get; set; }

        public Position Position { get; set; }
        public Faction Faction { get; set; }
        public BattleActor Actor { get; set; }

        public Action<BattleSquare> OnLeftClick { get; set; }
        public Action<BattleSquare> OnRightClick { get; set; }
        public Action<BattleSquare> OnHover { get; set; }
        public Action<BattleSquare> OnBlur { get; set; }

        private bool _wasHovered;

        public BattleSquare(Position p, Faction f, int x, int y, int width, int height)
        {
            Rectangle = new RectangleF(x, y, width, height);
            Position = p;
            Faction = f;
        }

        public override void onAddedToEntity()
        {
            Sprite = entity.addComponent(new Sprite(CommonResources.Pixel));
            Sprite.setColor(new Color(75, 75, 75, 75));
            Sprite.renderLayer = 6;
            Sprite.origin = Vector2.Zero;
            Sprite.enabled = false;
            transform.setScale(Rectangle.width);
            transform.setPosition(Rectangle.x, Rectangle.y);
        }

        public void update()
        {
            var mousePos = Input.mousePosition;
            if(Rectangle.contains(mousePos))
            {
                _wasHovered = true;
                OnHover?.Invoke(this);
                if (VirtualButtons.MouseLeft.isPressed)
                {
                    OnLeftClick?.Invoke(this);
                }
                else if (VirtualButtons.MouseRight.isPressed)
                {
                    OnRightClick?.Invoke(this);
                }
            }
            else if(_wasHovered)
            {
                _wasHovered = false;
                OnBlur?.Invoke(this);
            }
        }

        public void Reset()
        {
            OnBlur = null;
            OnHover = null;
            OnLeftClick = null;
            OnRightClick = null;
            Sprite.enabled = false;
        }
    }
}
