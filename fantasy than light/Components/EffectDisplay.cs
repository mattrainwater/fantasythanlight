﻿using Microsoft.Xna.Framework;
using Nez;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FantasyThanLight.Components
{
    public class EffectDisplay : Text, IUpdatable
    {
        public float TimeToLive { get; set; }

        private float _elapsedTIme;
        private EffectDisplayContainer _container;

        public EffectDisplay(EffectDisplayContainer container, IFont font, string text, Vector2 position, Color color, float timeToLive) 
            : base(font, text, position, color)
        {
            TimeToLive = timeToLive;
            _container = container;
            renderLayer = -5;
        }

        public void update()
        {
            _elapsedTIme += Time.deltaTime;
            if (_elapsedTIme >= TimeToLive)
            {
                entity.destroy();
                _container.Remove(this);
            }
        }
    }
}
