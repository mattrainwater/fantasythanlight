﻿using FantasyThanLight.Battle;
using FantasyThanLight.Battle.Phases;
using FantasyThanLight.Common;
using Nez;
using Nez.Sprites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FantasyThanLight.Components
{
    public class CardBattleController : Component, IUpdatable
    {
        public const int HORIZONTAL_OFFSET = 96;
        public const int VERTICAL_OFFSET = 64;
        public const int CHARACTER_GRID = 64;

        public List<BattleSquare> Squares { get; set; }
        public Player Player { get; set; }
        public Player EnemyPlayer { get; set; }

        private List<Turn> _turns { get; set; }
        private Turn _currentTurn { get; set; }
        private bool _initialized { get; set; }
        private bool _paused { get; set; }

        public CardBattleController(Player player, Player enemyPlayer)
        {
            Player = player;
            EnemyPlayer = enemyPlayer;
            Squares = new List<BattleSquare>();
            _turns = GetTurns();
        }

        public override void onAddedToEntity()
        {
            var rows = Enum.GetValues(typeof(Row)).Cast<Row>();
            var columns = Enum.GetValues(typeof(Column)).Cast<Column>();
            foreach (var row in rows)
            {
                foreach (var column in columns)
                {
                    var playerSquareEntity = entity.scene.createEntity("square");
                    var enemySquareEntity = entity.scene.createEntity("square");

                    var playerX = HORIZONTAL_OFFSET + ((int)column * CHARACTER_GRID);
                    var enemyX = DimensionConstants.DESIGN_WIDTH - HORIZONTAL_OFFSET - (((int)column + 1) * CHARACTER_GRID);
                    var y = VERTICAL_OFFSET + ((int)row * CHARACTER_GRID);
                    Squares.Add(playerSquareEntity.addComponent(new BattleSquare(new Position(row, column), Faction.Player, playerX, y, CHARACTER_GRID, CHARACTER_GRID)));
                    Squares.Add(enemySquareEntity.addComponent(new BattleSquare(new Position(row, column), Faction.Enemy, enemyX, y, CHARACTER_GRID, CHARACTER_GRID)));
                }
            }
        }

        public void update()
        {
            if (_initialized && !_paused)
            {
                if (_currentTurn == null || _currentTurn.IsFinished)
                {
                    _currentTurn = GetNextTurn(_currentTurn);
                    _currentTurn.Initialize();
                }
                _currentTurn.Update();
            }
        }

        public void Initialize()
        {
            // each player draws hand size
            for(var i = 0;  i < 3; i++)
            {
                var topOfDeck = Player.Deck.First();
                var cardEntity = entity.scene.createEntity("playerCardInHand");
                var frontSprite = cardEntity.addComponent(new Sprite(CommonResources.CardFront));
                frontSprite.enabled = false;
                var backSprite = cardEntity.addComponent(new Sprite(CommonResources.CardBack));
            }
            _initialized = true;
        }

        private Turn GetNextTurn(Turn currentTurn)
        {
            if (currentTurn == null)
            {
                return _turns.First();
            }
            else
            {
                var index = _turns.IndexOf(currentTurn);
                index = index == _turns.Count() - 1 ? 0 : index + 1;
                return _turns[index];
            }
        }

        private List<Turn> GetTurns()
        {
            return new List<Turn>
            {
                new Turn
                {
                    Column = Column.Vanguard,
                    Controller = this,
                    CurrentPlayer = Player,
                    OtherPlayer = EnemyPlayer,
                },
                new Turn
                {
                    Column = Column.Vanguard,
                    Controller = this,
                    CurrentPlayer = EnemyPlayer,
                    OtherPlayer = Player,
                },
                new Turn
                {
                    Column = Column.Flank,
                    Controller = this,
                    CurrentPlayer = Player,
                    OtherPlayer = EnemyPlayer,
                },
                new Turn
                {
                    Column = Column.Flank,
                    Controller = this,
                    CurrentPlayer = EnemyPlayer,
                    OtherPlayer = Player,
                },
                new Turn
                {
                    Column = Column.Rear,
                    Controller = this,
                    CurrentPlayer = Player,
                    OtherPlayer = EnemyPlayer,
                },
                new Turn
                {
                    Column = Column.Rear,
                    Controller = this,
                    CurrentPlayer = EnemyPlayer,
                    OtherPlayer = Player,
                },
            };
        }
    }
}
