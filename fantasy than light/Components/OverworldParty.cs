﻿using FantasyThanLight.Common;
using FantasyThanLight.Scenes;
using FantasyThanLight.State;
using Microsoft.Xna.Framework;
using Nez;
using Nez.Tiled;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FantasyThanLight.Components
{
    public class OverworldParty : Component, IUpdatable
    {
        public Point Position { get; set; }
        public bool IsMoving { get; set; }
        public TiledMap Map { get; set; }

        private Point? _nextPosition;
        private Vector2? _nextWorldPosition;
        private Vector2? _previousWorldPosition;
        private const float _speed = 50;
        private float _distance;
        private Vector2? _direction;
        private Dictionary<string, string> _nextTileProps;
        private Action<Dictionary<string, string>> _nextOnFinishMove;

        public OverworldParty(PartyState party, MapState map)
        {
            Position = party.OverworldPosition;
            Map = map.TiledMap;
        }

        public override void onAddedToEntity()
        {
            entity.setPosition(GetWorldCoordinatesFromPosition(Position));
            var followCamera = entity.getComponent<FollowCamera>();
            followCamera.camera.setPosition(entity.position);
        }

        public void SetPosition(Point tilePos, Action<Dictionary<string, string>> onFinishMove, Dictionary<string, string> tileProps)
        {
            IsMoving = true;
            _nextPosition = tilePos;
            _nextWorldPosition = GetWorldCoordinatesFromPosition(tilePos);
            _previousWorldPosition = entity.position;
            _distance = Vector2.Distance(entity.position, _nextWorldPosition.Value);
            _direction = Vector2.Normalize(_nextWorldPosition.Value - entity.position);
            _nextOnFinishMove = onFinishMove;
            _nextTileProps = tileProps;
        }

        public void update()
        {
            if (IsMoving)
            {
                var displacement = Vector2Ext.round(_direction.Value * _speed * Time.altDeltaTime);
                var nextPosition = entity.position + displacement;
                if (Vector2.Distance(_previousWorldPosition.Value, nextPosition) >= _distance)
                {
                    Position = _nextPosition.Value;
                    GlobalState.Party.OverworldPosition = Position;
                    entity.position = _nextWorldPosition.Value;
                    IsMoving = false;
                    _nextOnFinishMove(_nextTileProps);
                }
                else
                {
                    entity.position = nextPosition;
                }
            }
        }

        private Vector2 GetWorldCoordinatesFromPosition(Point pos)
        {
            var newPosition = Map.hexagonalTileToWorldPosition(pos);
            return newPosition;
        }
    }
}
