﻿using FantasyThanLight.Common;
using Microsoft.Xna.Framework;
using Nez;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FantasyThanLight.Components
{
    public class EffectDisplayContainer : Component
    {
        public List<EffectDisplay> EffectDisplays { get; set; }

        public EffectDisplayContainer()
        {
            EffectDisplays = new List<EffectDisplay>();
        }

        public void MakeEffectDisplay(string text, Color color, float ttl)
        {
            var scene = entity.scene;
            var display = scene.addEntity(new Entity());
            display.setScale(2f);
            var xPos = entity.transform.position.X + (DimensionConstants.CHARACTER_WIDTH) + 3;
            var yPos = entity.transform.position.Y - (EffectDisplays.Count * 20) - 2;
            var effectDisplay = new EffectDisplay(this, CommonResources.DefaultBitmapFont, text, new Vector2(xPos, yPos), color, ttl);
            display.addComponent(effectDisplay);
            EffectDisplays.Add(effectDisplay);
        }

        public void Remove(EffectDisplay display)
        {
            EffectDisplays.Remove(display);
            foreach (var effectDisplay in EffectDisplays)
            {
                effectDisplay.transform.position = new Vector2(effectDisplay.transform.position.X, effectDisplay.transform.position.Y + 10);
            }
        }
    }
}
