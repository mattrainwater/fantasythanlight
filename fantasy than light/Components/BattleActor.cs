﻿using System;
using Nez;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using FantasyThanLight.Battle;
using Microsoft.Xna.Framework.Graphics;
using FantasyThanLight.Common;
using System.Linq;
using FantasyThanLight.State;
using Nez.Sprites;

namespace FantasyThanLight.Components
{
    public class BattleActor : Component, IUpdatable
    {
        public int MaxHP { get; set; }
        public int CurrentHP { get; set; }
        public Dictionary<Column, Attack> Attacks { get; set; }
        public Dictionary<Column, List<Ability>> Abilities { get; set; }
        public Faction Faction { get; set; }
        public bool IsDead { get; set; }
        public int Armor { get; set; }
        public int Speed { get; set; }
        public int Resistance { get; set; }
        public bool IsFinished { get; set; }
        public Position Position { get; set; }
        public CharacterState Character { get; set; }
        public StunStatus Stun { get; set; }

        public List<StatusEffect> StatusEffects { get; set; }

        public BattleController Controller { get; set; }
        public FillupBar HPBar { get; set; }
        public EffectDisplayContainer EffectDisplayContainer { get; set; }
        public FillupBar TurnMarker { get; set; }

        public BattleActor(MemberState member, CharacterState character, Position pos = null)
        {
            Attacks = character.Attacks;
            Abilities = character.Abilities;
            IsDead = false;
            Armor = character.Armor;
            Speed = character.Speed;
            Resistance = character.Resistance;
            MaxHP = character.MaxHP;
            IsFinished = false;
            Character = character;

            CurrentHP = member!= null ? member.CurrentHP : character.MaxHP;
            Position = pos != null ? pos : member.PlayerSetPosition;

            StatusEffects = new List<StatusEffect>();
        }

        public void update()
        {
            var percHP = (float)CurrentHP / MaxHP;
            HPBar.CurrentWidth = percHP * HPBar.MaxWidth;
        }

        public void ExecuteAttack()
        {
            if(TurnMarker == null)
            {
                TurnMarker = entity.addComponent(new FillupBar(2, 32, Color.LightGoldenrodYellow, Color.LightGoldenrodYellow));
                TurnMarker.localOffset = new Vector2(16, 60);
            }
            TurnMarker.enabled = true;
            IsFinished = false;
            var currentAttack = Attacks[Position.Column];
            Controller.AttackName.setText(currentAttack.Name);
            currentAttack.Apply(Controller, this);
            Core.schedule(BattleController.TURN_LENGTH, (x) => {
                Controller.AttackName.setText("");
                IsFinished = true;
                TurnMarker.enabled = false;
            });
        }

        public void RemoveFromSquares()
        {
            foreach(var square in Controller.Squares)
            {
                if(square.Actor == this)
                {
                    square.Actor = null;
                }
            }
        }

        public void Move(Position nextPosition)
        {
            var square = GetSquareFromPosition(nextPosition);
            RemoveFromSquares();
            square.Actor = this;
            entity.transform.setPosition(square.Rectangle.x, square.Rectangle.y);
            Position = nextPosition;
        }

        private BattleSquare GetSquareFromPosition(Position nextPosition)
        {
            return Controller.Squares.First(x => x.Position == nextPosition && x.Faction == Faction);
        }

        public void ShowTargets()
        {
            var currentAttack = Attacks[Position.Column];
            var targets = currentAttack.GetTargets(Controller, this);
            foreach(var target in targets)
            {
                var square = Controller.Squares.First(x => x.Actor == target);
                var sprite = square.getComponent<Sprite>();
                if(sprite != null)
                {
                    sprite.enabled = true;
                    sprite.setColor(new Color(255, 0, 0, 200));
                }
            }
        }

        public void ClearTargets()
        {
            var currentAttack = Attacks[Position.Column];
            var targets = currentAttack.GetTargets(Controller, this);
            foreach (var target in targets)
            {
                var square = Controller.Squares.First(x => x.Actor == target);
                var sprite = square.getComponent<Sprite>();
                if (sprite != null)
                {
                    sprite.enabled = false;
                }
            }
        }
    }
}