﻿using FantasyThanLight.Battle;
using Nez;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FantasyThanLight.Components
{
    public class CardActor : Component
    {
        public int MaxHP { get; set; }
        public int CurrentHP { get; set; }
        public int Strength { get; set; }
        public Faction Faction { get; set; }
        public Position Position { get; set; }
        public Dictionary<Column, Attack> SpecialActions { get; set; }
        public Dictionary<Column, List<Ability>> PassiveAbilities { get; set; }
    }
}
