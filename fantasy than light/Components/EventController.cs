﻿using FantasyThanLight.Common;
using FantasyThanLight.Events;
using Microsoft.Xna.Framework.Input;
using Nez;
using Nez.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FantasyThanLight.Components
{
    public class EventController : Component, IUpdatable
    {
        public Dialog DialogBox { get; set; }
        public bool IsOpen { get; set; }

        private const int MAX_CHARS_PER_LINE = 80;

        private Stage _stage;
        private TextButtonStyle _buttonStyle;
        private List<Action<Button>> _buttonOnEnters;

        public EventController(Stage stage, Dialog dialog, TextButtonStyle style)
        {
            _stage = stage;
            DialogBox = dialog;
            _buttonStyle = style;
            DialogBox.setVisible(false);
            DialogBox.setMovable(false);
            DialogBox.setResizable(false);
            DialogBox.contentTable.defaults().space(1);
            DialogBox.buttonTable.defaults().space(1);
            DialogBox.buttonTable.left();

            _buttonOnEnters = new List<Action<Button>>();
        }

        public void update()
        {
            int indexOfPressedKey = -1;
            for(var i = 0; i < VirtualButtons.DInputs.Count; i++)
            {
                var input = VirtualButtons.DInputs[i];
                if(input.isPressed)
                {
                    indexOfPressedKey = i;
                    break;
                }
            }
            if(indexOfPressedKey != -1)
            {
                indexOfPressedKey--;
                if(indexOfPressedKey >= 0 && indexOfPressedKey < _buttonOnEnters.Count)
                {
                    _buttonOnEnters[indexOfPressedKey](null);
                }
            }
        }

        public void FireEvent(Dictionary<string, string> tileProps)
        {
            var nextEvent = new Event()
            {
                Choices = new List<Choice>
                {
                    new Choice()
                    {
                        Text = "Choice one!",
                        NextEvent = new Event()
                        {
                            Title = "You picked one!",
                            Text = "Blah!",
                            Choices = new List<Choice>()
                            {
                                new Choice()
                                {
                                    Text = "Done!",
                                    EventCommands = "addgold 2; removefood 2;"
                                }
                            }
                        }
                    },
                    new Choice()
                    {
                        Text = "Two!"
                    },
                    new Choice()
                    {
                        Text = "And three!"
                    },
                },
                Title = "Title of the Event",
                Text = "This is a test event! Nothing but a test here! Nopers! This is a test event! Nothing but a test here! Nopers! This is a test event! Nothing but a test here! Nopers! This is a test event! Nothing but a test here! Nopers! This is a test event! Nothing but a test here! Nopers! This is a test event! Nothing but a test here! Nopers! This is a test event! Nothing but a test here! Nopers! This is a test event! Nothing but a test here! Nopers! This is a test event! Nothing but a test here! Nopers! This is a test event! Nothing but a test here! Nopers! This is a test event! Nothing but a test here! Nopers! This is a test event! Nothing but a test here! Nopers! This is a test event! Nothing but a test here! Nopers! This is a test event! Nothing but a test here! Nopers! This is a test event! Nothing but a test here! Nopers! This is a test event! Nothing but a test here! Nopers!"
            };
            LoadEvent(nextEvent);
        }

        private void LoadEvent(Event nextEvent)
        {
            DialogBox.getTitleLabel().setText(nextEvent.Title);
            DialogBox.getContentTable().clearChildren();
            DialogBox.addText(nextEvent.Text.FormatRawText(MAX_CHARS_PER_LINE, new char[1] { ' ' }));
            DialogBox.getButtonTable().clearChildren();
            _buttonOnEnters.Clear();
            for (var i = 0; i < nextEvent.Choices.Count; i++)
            {
                var choice = nextEvent.Choices[i];
                var button = DialogBox.buttonTable.add(new TextButton((i + 1) + ". " + choice.Text, _buttonStyle))
                    .setFillX()
                    .setExpandX()
                    .getElement<TextButton>();
                Action<Button> onEnter = b =>
                {
                    if(!string.IsNullOrEmpty(choice.EventCommands))
                    {
                        EventParser.ParseEventCommands(choice.EventCommands);
                    }

                    if(choice.NextEvent != null)
                    {
                        LoadEvent(choice.NextEvent);
                    }
                    else
                    {
                        DialogBox.setVisible(false);
                        IsOpen = false;
                    }
                };
                button.onClicked += onEnter;
                _buttonOnEnters.Add(onEnter);
                DialogBox.buttonTable.row().setPadTop(0).setPadBottom(0);
            }
            IsOpen = true;
            DialogBox.setVisible(true);
        }

    }
}