﻿using Nez.Tiled;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FantasyThanLight.State
{
    public class MapState
    {
        public TiledMap TiledMap { get; set; }
        public TiledTile[] Tiles { get; set; }
    }
}
