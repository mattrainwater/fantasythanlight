﻿using FantasyThanLight.Battle;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace FantasyThanLight.State
{
    public class CharacterState
    {
        public int CharacterId { get; set; }
        public string Name { get; set; }
        public Dictionary<Column, Attack> Attacks { get; set; }
        public Dictionary<Column, List<Ability>> Abilities { get; set; }
        public int Armor { get; set; }
        public int Speed { get; set; }
        public int Resistance { get; set; }
        public int MaxHP { get; set; }
        public string Sprite { get; set; }
    }
}