﻿using FantasyThanLight.TileGeneration;
using Microsoft.Xna.Framework.Graphics;
using Nez.TextureAtlases;
using Nez.Textures;
using Nez.Tiled;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FantasyThanLight.State
{
    public static class GlobalState
    {
        public static PartyState Party = new PartyState();
        public static MapState Map = new MapState();
        public static List<CharacterState> Characters = new List<CharacterState>();
        public static TileConfig TileConfig;
        public static TextureAtlas UnitSprites;
    }
}
