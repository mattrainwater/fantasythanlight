﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FantasyThanLight.State
{
    public class PartyState
    {
        public PartyState()
        {
            Members = new List<MemberState>();
        }

        public List<MemberState> Members { get; set; }
        public Point OverworldPosition { get; set; }
        public int Gold { get; set; }
        public int Food { get; set; }
    }
}
