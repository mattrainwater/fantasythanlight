﻿using FantasyThanLight.Battle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FantasyThanLight.State
{
    public class MemberState
    {
        public string Name { get; set; }
        public int CharacterId { get; set; }
        public int CurrentHP { get; set; }
        public Position PlayerSetPosition { get; set; }
    }
}
