<?xml version="1.0" encoding="UTF-8"?>
<tileset name="fantasyhextiles_v2" tilewidth="32" tileheight="48" tilecount="40" columns="8">
 <image source="tiles.png" width="256" height="240"/>
 <tile id="0">
  <properties>
   <property name="main" value="field"/>
   <property name="terrain" value="field"/>
   <property name="type" value="main"/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="main" value="field"/>
   <property name="terrain" value="woods"/>
   <property name="type" value="flavor"/>
  </properties>
 </tile>
 <tile id="2">
  <properties>
   <property name="main" value="field"/>
   <property name="terrain" value="thick_woods"/>
   <property name="type" value="flavor"/>
  </properties>
 </tile>
 <tile id="3">
  <properties>
   <property name="main" value="field"/>
   <property name="terrain" value="hills"/>
   <property name="type" value="hills"/>
  </properties>
 </tile>
 <tile id="4">
  <properties>
   <property name="main" value="field"/>
   <property name="terrain" value="wooded_hills"/>
   <property name="type" value="hills"/>
  </properties>
 </tile>
 <tile id="5">
  <properties>
   <property name="terrain" value="mountain"/>
   <property name="type" value="mountain"/>
  </properties>
 </tile>
 <tile id="6">
  <properties>
   <property name="terrain" value="shallow_ocean"/>
   <property name="type" value="shallow_ocean"/>
  </properties>
 </tile>
 <tile id="7">
  <properties>
   <property name="terrain" value="ocean"/>
   <property name="type" value="ocean"/>
  </properties>
 </tile>
 <tile id="8">
  <properties>
   <property name="main" value="field"/>
   <property name="terrain" value="village"/>
   <property name="type" value="city"/>
  </properties>
 </tile>
 <tile id="9">
  <properties>
   <property name="main" value="field"/>
   <property name="terrain" value="town"/>
   <property name="type" value="city"/>
  </properties>
 </tile>
 <tile id="10">
  <properties>
   <property name="main" value="field"/>
   <property name="terrain" value="city"/>
   <property name="type" value="city"/>
  </properties>
 </tile>
 <tile id="11">
  <properties>
   <property name="main" value="field"/>
   <property name="terrain" value="farmland"/>
   <property name="type" value="special"/>
  </properties>
 </tile>
 <tile id="12">
  <properties>
   <property name="main" value="scrubland"/>
   <property name="terrain" value="bayou"/>
   <property name="type" value="flavor"/>
  </properties>
 </tile>
 <tile id="13">
  <properties>
   <property name="main" value="scrubland"/>
   <property name="terrain" value="swamp"/>
   <property name="type" value="flavor"/>
  </properties>
 </tile>
 <tile id="14">
  <properties>
   <property name="main" value="scrubland"/>
   <property name="terrain" value="scrubland"/>
   <property name="type" value="main"/>
  </properties>
 </tile>
 <tile id="15">
  <properties>
   <property name="main" value="scrubland"/>
   <property name="terrain" value="meadow"/>
   <property name="type" value="special"/>
  </properties>
 </tile>
 <tile id="16">
  <properties>
   <property name="main" value="snow"/>
   <property name="terrain" value="snow"/>
   <property name="type" value="main"/>
  </properties>
 </tile>
 <tile id="17">
  <properties>
   <property name="main" value="snow"/>
   <property name="terrain" value="snow_woods"/>
   <property name="type" value="flavor"/>
  </properties>
 </tile>
 <tile id="18">
  <properties>
   <property name="main" value="snow"/>
   <property name="terrain" value="snow_thick_woods"/>
   <property name="type" value="flavor"/>
  </properties>
 </tile>
 <tile id="19">
  <properties>
   <property name="main" value="snow"/>
   <property name="terrain" value="snow_hills"/>
   <property name="type" value="hills"/>
  </properties>
 </tile>
 <tile id="20">
  <properties>
   <property name="main" value="snow"/>
   <property name="terrain" value="snow_wooded_hills"/>
   <property name="type" value="hills"/>
  </properties>
 </tile>
 <tile id="21">
  <properties>
   <property name="main" value="snow"/>
   <property name="terrain" value="icy_ocean"/>
   <property name="type" value="special"/>
  </properties>
 </tile>
 <tile id="22">
  <properties>
   <property name="main" value="snow"/>
   <property name="terrain" value="snow_village"/>
   <property name="type" value="city"/>
  </properties>
 </tile>
 <tile id="23">
  <properties>
   <property name="main" value="snow"/>
   <property name="terrain" value="snow_castle"/>
   <property name="type" value="city"/>
  </properties>
 </tile>
 <tile id="24">
  <properties>
   <property name="main" value="desert"/>
   <property name="terrain" value="desert"/>
   <property name="type" value="main"/>
  </properties>
 </tile>
 <tile id="25">
  <properties>
   <property name="main" value="desert"/>
   <property name="terrain" value="desert_hills"/>
   <property name="type" value="hills"/>
  </properties>
 </tile>
 <tile id="26">
  <properties>
   <property name="main" value="desert"/>
   <property name="terrain" value="dunes"/>
   <property name="type" value="flavor"/>
  </properties>
 </tile>
 <tile id="27">
  <properties>
   <property name="main" value="desert"/>
   <property name="terrain" value="plateau"/>
   <property name="type" value="flavor"/>
  </properties>
 </tile>
 <tile id="28">
  <properties>
   <property name="main" value="desert"/>
   <property name="terrain" value="oasis"/>
   <property name="type" value="special"/>
  </properties>
 </tile>
 <tile id="29">
  <properties>
   <property name="main" value="desert"/>
   <property name="terrain" value="desert_village"/>
   <property name="type" value="city"/>
  </properties>
 </tile>
 <tile id="30">
  <properties>
   <property name="main" value="desert"/>
   <property name="terrain" value="desert_town"/>
   <property name="type" value="city"/>
  </properties>
 </tile>
 <tile id="31">
  <properties>
   <property name="main" value="desert"/>
   <property name="terrain" value="desert_city"/>
   <property name="type" value="city"/>
  </properties>
 </tile>
 <tile id="32">
  <properties>
   <property name="main" value="scrubland"/>
   <property name="terrain" value="jungle"/>
   <property name="type" value="flavor"/>
  </properties>
 </tile>
 <tile id="33">
  <properties>
   <property name="terrain" value="cave"/>
  </properties>
 </tile>
 <tile id="34">
  <properties>
   <property name="terrain" value="snow_cave"/>
  </properties>
 </tile>
 <tile id="35">
  <properties>
   <property name="terrain" value="desert_cave"/>
  </properties>
 </tile>
</tileset>
