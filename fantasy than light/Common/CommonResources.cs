﻿using Microsoft.Xna.Framework.Graphics;
using Nez.BitmapFonts;
using Nez.Textures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FantasyThanLight.Common
{
    public static class CommonResources
    {
        public static BitmapFont DefaultBitmapFont { get; set; }

        public static Texture2D Pixel { get; set; }
        public static Texture2D CardBack { get; set; }
        public static Texture2D CardFront { get; set; }
    }
}
