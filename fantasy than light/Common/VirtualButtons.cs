﻿using Microsoft.Xna.Framework.Input;
using Nez;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FantasyThanLight.Common
{
    public static class VirtualButtons
    {
        public static VirtualButton SelectInput;
        public static VirtualButton BackInput;
        public static VirtualButton LeftInput;
        public static VirtualButton RightInput;
        public static VirtualButton UpInput;
        public static VirtualButton DownInput;

        public static VirtualButton MouseLeft;
        public static VirtualButton MouseRight;

        public static VirtualButton D0Input;
        public static VirtualButton D1Input;
        public static VirtualButton D2Input;
        public static VirtualButton D3Input;
        public static VirtualButton D4Input;
        public static VirtualButton D5Input;
        public static VirtualButton D6Input;
        public static VirtualButton D7Input;
        public static VirtualButton D8Input;
        public static VirtualButton D9Input;

        public static List<VirtualButton> DInputs;

        public static void SetupInput()
        {
            SelectInput = new VirtualButton();
            SelectInput.nodes.Add(new Nez.VirtualButton.KeyboardKey(Keys.J));
            SelectInput.nodes.Add(new Nez.VirtualButton.GamePadButton(0, Buttons.A));

            BackInput = new VirtualButton();
            BackInput.nodes.Add(new Nez.VirtualButton.KeyboardKey(Keys.L));
            BackInput.nodes.Add(new Nez.VirtualButton.GamePadButton(0, Buttons.B));

            LeftInput = new VirtualButton();
            LeftInput.nodes.Add(new Nez.VirtualButton.KeyboardKey(Keys.A));
            LeftInput.nodes.Add(new Nez.VirtualButton.KeyboardKey(Keys.Left));
            LeftInput.nodes.Add(new Nez.VirtualButton.GamePadButton(0, Buttons.DPadLeft));
            LeftInput.nodes.Add(new Nez.VirtualButton.GamePadButton(0, Buttons.LeftThumbstickLeft));

            RightInput = new VirtualButton();
            RightInput.nodes.Add(new Nez.VirtualButton.KeyboardKey(Keys.D));
            RightInput.nodes.Add(new Nez.VirtualButton.KeyboardKey(Keys.Right));
            RightInput.nodes.Add(new Nez.VirtualButton.GamePadButton(0, Buttons.DPadRight));
            RightInput.nodes.Add(new Nez.VirtualButton.GamePadButton(0, Buttons.LeftThumbstickRight));

            UpInput = new VirtualButton();
            UpInput.nodes.Add(new Nez.VirtualButton.KeyboardKey(Keys.W));
            UpInput.nodes.Add(new Nez.VirtualButton.KeyboardKey(Keys.Up));
            UpInput.nodes.Add(new Nez.VirtualButton.GamePadButton(0, Buttons.DPadUp));
            UpInput.nodes.Add(new Nez.VirtualButton.GamePadButton(0, Buttons.LeftThumbstickUp));

            DownInput = new VirtualButton();
            DownInput.nodes.Add(new Nez.VirtualButton.KeyboardKey(Keys.S));
            DownInput.nodes.Add(new Nez.VirtualButton.KeyboardKey(Keys.Down));
            DownInput.nodes.Add(new Nez.VirtualButton.GamePadButton(0, Buttons.DPadDown));
            DownInput.nodes.Add(new Nez.VirtualButton.GamePadButton(0, Buttons.LeftThumbstickDown));

            MouseLeft = new VirtualButton();
            MouseLeft.addMouseLeftButton();

            MouseRight = new VirtualButton();
            MouseRight.addMouseRightButton();

            D0Input = new VirtualButton();
            D0Input.nodes.Add(new Nez.VirtualButton.KeyboardKey(Keys.D0));

            D1Input = new VirtualButton();
            D1Input.nodes.Add(new Nez.VirtualButton.KeyboardKey(Keys.D1));

            D2Input = new VirtualButton();
            D2Input.nodes.Add(new Nez.VirtualButton.KeyboardKey(Keys.D2));

            D3Input = new VirtualButton();
            D3Input.nodes.Add(new Nez.VirtualButton.KeyboardKey(Keys.D3));

            D4Input = new VirtualButton();
            D4Input.nodes.Add(new Nez.VirtualButton.KeyboardKey(Keys.D4));

            D5Input = new VirtualButton();
            D5Input.nodes.Add(new Nez.VirtualButton.KeyboardKey(Keys.D5));

            D6Input = new VirtualButton();
            D6Input.nodes.Add(new Nez.VirtualButton.KeyboardKey(Keys.D6));

            D7Input = new VirtualButton();
            D7Input.nodes.Add(new Nez.VirtualButton.KeyboardKey(Keys.D7));

            D8Input = new VirtualButton();
            D8Input.nodes.Add(new Nez.VirtualButton.KeyboardKey(Keys.D8));

            D9Input = new VirtualButton();
            D9Input.nodes.Add(new Nez.VirtualButton.KeyboardKey(Keys.D9));

            DInputs = new List<VirtualButton>
            {
                D0Input,
                D1Input,
                D2Input,
                D3Input,
                D4Input,
                D5Input,
                D6Input,
                D7Input,
                D8Input,
                D9Input,
            };
        }
    }
}
