﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FantasyThanLight.Common;
using FantasyThanLight.State;

namespace FantasyThanLight.Events
{
    public static class EventParser
    {
        public static void ParseEventCommands(string eventCommands)
        {
            var commandSplit = eventCommands.Explode(new char[1] { ';' }).Select(x => x.Trim()).ToList();
            commandSplit.ForEach(ParseEventCommand);
        }

        private static void ParseEventCommand(string eventCommand)
        {
            var cleaned = eventCommand.Replace(";", "");
            var split = cleaned.Explode(new char[1] { ' ' });
            split = split.Where(x => !string.IsNullOrWhiteSpace(x)).ToArray();

            // addgold X
            // add X gold to party
            if(split.Count() == 2 && split[0] == "addgold")
            {
                var goldAdded = Int32.Parse(split[1]);
                GlobalState.Party.Gold += goldAdded;
            }

            // removegold X
            // remove X gold from party
            if (split.Count() == 2 && split[0] == "removegold")
            {
                var goldRemoved = Int32.Parse(split[1]);
                GlobalState.Party.Gold -= goldRemoved;
            }

            // addfood X
            // add X food to party
            if (split.Count() == 2 && split[0] == "addfood")
            {
                var foodAdded = Int32.Parse(split[1]);
                GlobalState.Party.Food += foodAdded;
            }

            // removefood X
            // remove X food from party
            if (split.Count() == 2 && split[0] == "removefood")
            {
                var foodRemoved = Int32.Parse(split[1]);
                GlobalState.Party.Food -= foodRemoved;
            }

            // startbattle X Y Z...
            // X is class id, Y is row, Z is column, can repeat as necessary

            // additem X
            // X is item id
        }
    }
}
