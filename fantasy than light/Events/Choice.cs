﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FantasyThanLight.Events
{
    public class Choice
    {
        public string Text { get; set; }
        public Event NextEvent { get; set; }
        public string EventCommands { get; set; }
    }
}
