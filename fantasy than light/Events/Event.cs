﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FantasyThanLight.Events
{
    public class Event
    {
        public Event()
        {
            Choices = new List<Choice>();
        }

        public string Title { get; set; }
        public string Text { get; set; }
        public List<Choice> Choices { get; set; }
    }
}
