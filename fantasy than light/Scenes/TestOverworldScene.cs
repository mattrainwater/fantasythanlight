﻿using FantasyThanLight.Common;
using FantasyThanLight.Components;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Newtonsoft.Json;
using Nez;
using Nez.Sprites;
using Nez.Textures;
using Nez.Tiled;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FantasyThanLight.TileGeneration;
using FantasyThanLight.State;
using Nez.UI;

namespace FantasyThanLight.Scenes
{
    public class TestOverworldScene : BaseScene
    {
        private UICanvas _canvas;
        private Table _table;

        public TestOverworldScene()
            : base()
        {

        }

        public override void onStart()
        {
            var tiledMap = content.Load<TiledMap>("example01");

            if(GlobalState.Map.Tiles == null)
            {
                var gen = new TileGenerator();
                GlobalState.Map.Tiles = gen.GenerateTileLayer(tiledMap.tilesets[0], tiledMap.width, tiledMap.height);
                tiledMap.layers.Clear();
                var layer = tiledMap.createTileLayer("test", tiledMap.width, tiledMap.height, GlobalState.Map.Tiles);
                layer.offset = new Vector2(-14, 25);
                GlobalState.Map.TiledMap = tiledMap;
                GlobalState.Party = new PartyState()
                {
                    OverworldPosition = new Point(tiledMap.width / 2, tiledMap.height / 2)
                };
            }
            else
            {
                foreach(var tile in GlobalState.Map.Tiles)
                {
                    tile.tileset = tiledMap.tilesets[0];
                }
            }

            var entity = createEntity("map");
            entity.addComponent(new TiledMapComponent(GlobalState.Map.TiledMap));

            var warriorCharacter = GlobalState.Characters.First(x => x.CharacterId == 0);
            var party = createEntity("party");
            var followCamera = party.addComponent(new FollowCamera(party, camera));
            var partyComponent = party.addComponent(new OverworldParty(GlobalState.Party, GlobalState.Map));
            var partySprite = party.addComponent(new Sprite(GlobalState.UnitSprites.getSubtexture(warriorCharacter.Sprite)));
            partySprite.setRenderLayer(-1);

            _canvas = createEntity("ui").addComponent(new UICanvas());
            _canvas.renderLayer = SCREEN_SPACE_RENDER_LAYER;
            _table = _canvas.stage.addElement(new Table());
            _table.setFillParent(true).center();

            var dialogSprite = content.Load<Texture2D>("textbox");
            var nineslice = new NinePatchDrawable(dialogSprite, 32, 32, 32, 32);

            var upButton = new NinePatchDrawable(content.Load<Texture2D>("button"), 5, 5, 5, 5);
            var downButton = new NinePatchDrawable(content.Load<Texture2D>("downbutton"), 5, 5, 5, 5);
            var style = new TextButtonStyle(upButton, downButton, upButton);
            var dialog = new Dialog("", new WindowStyle(CommonResources.DefaultBitmapFont, Color.White, nineslice));
            dialog.setScale(.5f);
            _table.add(dialog);

            var eventControllerEntity = createEntity("event");
            var eventController = eventControllerEntity.addComponent(new EventController(_canvas.stage, dialog, style));

            var controller = new OverworldController(partyComponent, eventController, GlobalState.Map.TiledMap);
            var controllerEntity = createEntity("controller");
            controllerEntity.addComponent(controller);
        }
    }
}
