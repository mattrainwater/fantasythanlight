﻿using FantasyThanLight.Battle;
using FantasyThanLight.Common;
using FantasyThanLight.Components;
using FantasyThanLight.State;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Nez;
using Nez.Sprites;
using Nez.Tiled;
using Nez.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FantasyThanLight.Scenes
{
    public class TestOtherBattleScene : BaseScene
    {
        private CardBattleController _controller;
        private List<Position> _allPositions = new List<Position>()
        {
            new Position(Row.Top, Column.Vanguard),
            new Position(Row.Top, Column.Flank),
            new Position(Row.Top, Column.Rear),
            new Position(Row.Middle, Column.Vanguard),
            new Position(Row.Middle, Column.Flank),
            new Position(Row.Middle, Column.Rear),
            new Position(Row.Bottom, Column.Vanguard),
            new Position(Row.Bottom, Column.Flank),
            new Position(Row.Bottom, Column.Rear)
        };

        public TestOtherBattleScene()
            : base()
        {
        }

        public override void initialize()
        {
            base.initialize();
        }

        public override void onStart()
        {
            var map = content.Load<TiledMap>("battle");
            var entity = createEntity("map");
            var tiledMapComponent = entity.addComponent(new TiledMapComponent(map));
            tiledMapComponent.renderLayer = 1000;
            map.layers[0].scale = 2f;
            map.layers[1].scale = 2f;
            map.layers[2].scale = 2f;

            //var controllerEntity = createEntity("controller");
            //_controller = controllerEntity.addComponent(new CardBattleController(CreatePlayer(), CreateEnemyPlayer()));
            //controllerEntity.setScale(2f);

            //_controller.Initialize();
            var cardEntity = entity.scene.createEntity("playerCardInHand");
            var text = cardEntity.addComponent(new UpdatableText(CommonResources.DefaultBitmapFont, Vector2.Zero, Color.Black, () => { return cardEntity.position.ToString(); }));
            text.localOffset = new Vector2(25, 0);
            cardEntity.position = new Vector2(0, 100);
            var frontSprite = cardEntity.addComponent(new Sprite(CommonResources.CardFront));
            frontSprite.enabled = false;
            var backSprite = cardEntity.addComponent(new Sprite(CommonResources.CardBack));
            cardEntity.addComponent(new MoveToPoint(new Vector2(500, 100), 2, () => {
                frontSprite.enabled = true;
                backSprite.enabled = false;
            }));
        }

        private Player CreatePlayer()
        {
            return new Player(new List<Card> {
                    new Card()
                    {
                        Number = 1
                    },
                    new Card()
                    {
                        Number = 2
                    },
                    new Card()
                    {
                        Number = 3
                    },
                    new Card()
                    {
                        Number = 4
                    },
                    new Card()
                    {
                        Number = 5
                    },
                    new Card()
                    {
                        Number = 6
                    },
                    new Card()
                    {
                        Number = 7
                    },
                    new Card()
                    {
                        Number = 8
                    },
                    new Card()
                    {
                        Number = 9
                    },
                    new Card()
                    {
                        Number = 10
                    }
                },
                Faction.Player
            );
        }

        private Player CreateEnemyPlayer()
        {
            return new Player(new List<Card> {
                    new Card()
                    {
                        Number = 1
                    },
                    new Card()
                    {
                        Number = 2
                    },
                    new Card()
                    {
                        Number = 3
                    },
                    new Card()
                    {
                        Number = 4
                    },
                    new Card()
                    {
                        Number = 5
                    },
                    new Card()
                    {
                        Number = 6
                    },
                    new Card()
                    {
                        Number = 7
                    },
                    new Card()
                    {
                        Number = 8
                    },
                    new Card()
                    {
                        Number = 9
                    },
                    new Card()
                    {
                        Number = 10
                    }
                },
                Faction.Enemy
            );
        }
    }
}
