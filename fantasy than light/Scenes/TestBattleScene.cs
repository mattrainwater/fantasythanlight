﻿using FantasyThanLight.Battle;
using FantasyThanLight.Common;
using FantasyThanLight.Components;
using FantasyThanLight.State;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Nez;
using Nez.Sprites;
using Nez.Tiled;
using Nez.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FantasyThanLight.Scenes
{
    public class TestBattleScene : BaseScene
    {
        private BattleController _controller;
        private List<Position> _allPositions = new List<Position>()
        {
            new Position(Row.Top, Column.Vanguard),
            new Position(Row.Top, Column.Flank),
            new Position(Row.Top, Column.Rear),
            new Position(Row.Middle, Column.Vanguard),
            new Position(Row.Middle, Column.Flank),
            new Position(Row.Middle, Column.Rear),
            new Position(Row.Bottom, Column.Vanguard),
            new Position(Row.Bottom, Column.Flank),
            new Position(Row.Bottom, Column.Rear)
        };

        private UICanvas _canvas;
        private Table _table;

        public TestBattleScene()
            : base()
        {
        }

        public override void initialize()
        {
            base.initialize();
            setDesignResolution(DimensionConstants.DESIGN_WIDTH, DimensionConstants.DESIGN_HEIGHT, SceneResolutionPolicy.ShowAllPixelPerfect);
        }

        public override void onStart()
        {
            var map = content.Load<TiledMap>("battle");
            var entity = createEntity("map");
            var tiledMapComponent = entity.addComponent(new TiledMapComponent(map));
            tiledMapComponent.renderLayer = 1000;
            map.layers[0].scale = 2f;
            map.layers[1].scale = 2f;
            map.layers[2].scale = 2f;

            _canvas = createEntity("ui").addComponent(new UICanvas());
            _canvas.renderLayer = SCREEN_SPACE_RENDER_LAYER;
            _table = _canvas.stage.addElement(new Table());
            _table.setFillParent(true).center();

            var dialogSprite = content.Load<Texture2D>("textbox");
            var nineslice = new NinePatchDrawable(dialogSprite, 32, 32, 32, 32);
            nineslice.minHeight = 300;
            nineslice.minWidth = 300;

            var window = new Window("", new WindowStyle(CommonResources.DefaultBitmapFont, Color.White, nineslice));
            window.setMovable(false);
            window.setResizable(false);
            window.setIsVisible(false);
            _table.add(window);

            var controllerEntity = createEntity("controller");
            _controller = controllerEntity.addComponent(new BattleController(window));
            controllerEntity.setScale(2f);

            CreateRandomActor(Faction.Player);
            CreateRandomActor(Faction.Player);
            CreateRandomActor(Faction.Player);

            CreateRandomActor(Faction.Enemy);
            CreateRandomActor(Faction.Enemy);
            CreateRandomActor(Faction.Enemy);
        }

        private void CreateRandomActor(Faction faction)
        {
            var availablePositions = GetAvailablePositions(faction);
            var randomCharacter = GlobalState.Characters.PickRandom();
            var entity = createEntity("random-actor" + _controller.Actors.Count());
            var randomActor = new BattleActor(null, randomCharacter, availablePositions.PickRandom());
            entity.addComponent(randomActor);
            var randomSprite = entity.addComponent(new Sprite(GlobalState.UnitSprites.getSubtexture(randomCharacter.Sprite)));
            _controller.AddActor(randomActor, faction);
        }

        private List<Position> GetAvailablePositions(Faction faction)
        {
            var existingActors = _controller.Actors.Where(x => x.Faction == faction);
            var existingPositions = existingActors.Select(x => x.Position);
            return _allPositions.Where(x => !existingPositions.Any(y => x.Row == y.Row && x.Column == y.Column)).ToList();
        }
    }
}
