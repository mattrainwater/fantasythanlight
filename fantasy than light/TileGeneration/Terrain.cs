﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FantasyThanLight.TileGeneration
{
    public class Terrain
    {
        public Terrain()
        {
            HillTileIds = new List<int>();
            FlavorTileIds = new List<int>();
            CityTileIds = new List<int>();
        }

        public string MainName { get; set; }
        public int MainTileId { get; set; }
        public List<int> HillTileIds { get; set; }
        public List<int> FlavorTileIds { get; set; }
        public List<int> CityTileIds { get; set; }
        public int SpecialTileId { get; set; }
    }
}
