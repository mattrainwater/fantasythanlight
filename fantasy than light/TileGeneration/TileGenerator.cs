﻿using Nez.Tiled;
using System.Collections.Generic;
using System.Linq;
using Nez;
using Microsoft.Xna.Framework;
using System;
using FantasyThanLight.Common;
using FantasyThanLight.State;

namespace FantasyThanLight.TileGeneration
{
    public class TileGenerator
    {
        private readonly Dictionary<int, List<Point>> HEX_NEIGHBORS = new Dictionary<int, List<Point>> {
            {1, new List<Point> { new Point(0, -1), new Point(-1, 0), new Point(1, 0), new Point(1, -1), new Point(0, 1), new Point(-1, -1) } },
            {0, new List<Point> { new Point(0, -1), new Point(-1, 0), new Point(1, 0), new Point(-1, 1), new Point(0, 1), new Point(1, 1) } }
        };

        public TiledTile[] GenerateTileLayer(TiledTileset set, int width, int height)
        {
            GlobalState.TileConfig = GenerateTileConfig(set);
            var tiles = new List<TiledTile>();

            var center = new Point(width / 2, height / 2);
            for (var i = 0; i < width; i++)
            {
                for (var j = 0; j < height; j++)
                {
                    var distance = GetDistance(new Point(i, j), center);
                    var tileId = distance > ((width - 10) + Nez.Random.range(0, 4)) ? GlobalState.TileConfig.OceanTileId : GlobalState.TileConfig.MountainTileId;
                    tiles.Add(new TiledTile(tileId) { tileset = set, x = i, y = j });
                }
            }

            var islandTiles = tiles.Where(x => x.id == GlobalState.TileConfig.MountainTileId).ToList();
            for (var i = 0; i < 3; i++)
            {
                var isolatedTiles = islandTiles.Where(x => AreNeighborsWater(x, islandTiles)).ToList();
                isolatedTiles.ForEach(x => x.id = GlobalState.TileConfig.ShallowOceanTileId);
                islandTiles.RemoveAll(x => isolatedTiles.Contains(x));
            }

            var oceanTiles = tiles.Where(x => x.id == GlobalState.TileConfig.OceanTileId).ToList();
            MakeShallowTiles(tiles);

            var voronoiPoints = GenerateVoronoiPoints(islandTiles, 30);
            var voronoiPointsToTiles = GetTileDictionary(voronoiPoints, islandTiles);
            GenerateRandomTileIds(islandTiles, voronoiPointsToTiles);
            var mountainTiles = islandTiles.Count() / 100;
            for(var i = 0; i < mountainTiles; i++)
            {
                islandTiles.PickRandom().id = GlobalState.TileConfig.MountainTileId;
            }

            return tiles.ToArray();
        }

        private TileConfig GenerateTileConfig(TiledTileset set)
        {
            var config = new TileConfig();

            foreach(var tile in set.tiles)
            {
                var tileId = tile.id + 1;
                var props = tile.properties;
                if(props.ContainsKey("type"))
                {
                    var type = props["type"];
                    if(props.ContainsKey("main"))
                    {
                        var main = props["main"];
                        var terrain = config.Terrains.FirstOrDefault(x => x.MainName == main);
                        if(terrain == null)
                        {
                            terrain = new Terrain() { MainName = main };
                            config.Terrains.Add(terrain);
                        }
                        if (type == "main")
                        {
                            terrain.MainTileId = tileId;
                        }
                        if(type == "flavor")
                        {
                            terrain.FlavorTileIds.Add(tileId);
                        }
                        if(type == "hills")
                        {
                            terrain.HillTileIds.Add(tileId);
                        }
                        if (type == "city")
                        {
                            terrain.CityTileIds.Add(tileId);
                        }
                        if (type == "special")
                        {
                            terrain.SpecialTileId = tileId;
                        }
                    }
                    else
                    {
                        if(type == "mountain")
                        {
                            config.MountainTileId = tileId;
                        }
                        if(type == "shallow_ocean")
                        {
                            config.ShallowOceanTileId = tileId;
                        }
                        if(type == "ocean")
                        {
                            config.OceanTileId = tileId;
                        }
                    }
                }
            }

            return config;
        }

        private void MakeShallowTiles(List<TiledTile> tiles)
        {
            foreach(var oceanTile in tiles.Where(x => x.id == GlobalState.TileConfig.OceanTileId))
            {
                var isEven = oceanTile.x % 2;
                var validNeighbors = HEX_NEIGHBORS[isEven];
                var neighborPositions = validNeighbors.Select(x => x + new Point(oceanTile.x, oceanTile.y));
                var neighborTiles = tiles.Where(x => neighborPositions.Any(y => y == new Point(x.x, x.y)));
                if(neighborTiles.Any(x => x.id == GlobalState.TileConfig.MountainTileId))
                {
                    oceanTile.id = GlobalState.TileConfig.ShallowOceanTileId;
                }
            }
        }

        private bool AreNeighborsWater(TiledTile tile, List<TiledTile> tiles)
        {
            var isEven = tile.x % 2;
            var validNeighbors = HEX_NEIGHBORS[isEven];
            var neighborPositions = validNeighbors.Select(x => x + new Point(tile.x, tile.y));
            var neighborTiles = tiles.Where(x => neighborPositions.Any(y => y == new Point(x.x, x.y)));

            return neighborTiles.Count() <= 1;
        }

        private void GenerateRandomTileIds(List<TiledTile> tiles, Dictionary<Point, List<TiledTile>> voronoiPointsToTiles)
        {
            foreach(var voronoi in voronoiPointsToTiles.Keys.ToList())
            {
                var randomTerrain = GlobalState.TileConfig.Terrains.PickRandom();
                var tilesOfAType = voronoiPointsToTiles[voronoi];
                var tilesInMainList = tiles.Where(x => tilesOfAType.Any(y => y.x == x.x && y.y == x.y)).ToList();
                tilesInMainList.ForEach(x => x.id = randomTerrain.MainTileId);

                if(randomTerrain.HillTileIds.Any())
                {
                    var hillTiles = tilesInMainList.Count() / 5;
                    for (var i = 0; i < hillTiles; i++)
                    {
                        tilesInMainList.PickRandom().id = randomTerrain.HillTileIds.PickRandom();
                    }
                }

                if (randomTerrain.FlavorTileIds.Any())
                {
                    var flavorTiles = tilesInMainList.Count() / 3;
                    for (var i = 0; i < flavorTiles; i++)
                    {
                        tilesInMainList.PickRandom().id = randomTerrain.FlavorTileIds.PickRandom();
                    }
                }

                if (randomTerrain.CityTileIds.Any() && tilesInMainList.Count() >= 7)
                {
                    tilesInMainList.PickRandom().id = randomTerrain.CityTileIds.PickRandom();
                }
            }
        }

        private Dictionary<Point, List<TiledTile>> GetTileDictionary(List<Point> voronoiPoints, List<TiledTile> tiles)
        {
            var dict = new Dictionary<Point, List<TiledTile>>();

            foreach(var tile in tiles)
            {
                var closestPoint = GetClosestVoronoiPoint(voronoiPoints, tile.point);
                if(!dict.ContainsKey(closestPoint))
                {
                    dict.Add(closestPoint, new List<TiledTile>());
                }
                dict[closestPoint].Add(tile);
            }

            return dict;
        }

        private Point GetClosestVoronoiPoint(List<Point> voronoiPoints, Point tilePoint)
        {
            var minDistance = float.MaxValue;
            Point closestPoint = Point.Zero;
            foreach(var point in voronoiPoints)
            {
                var distance = GetDistance(point, tilePoint);
                if(distance < minDistance)
                {
                    minDistance = distance;
                    closestPoint = point;
                }
            }
            return closestPoint;
        }

        private float GetDistance(Point aPoint, Point bPoint)
        {
            var a = ConvertToCube(aPoint);
            var b = ConvertToCube(bPoint);

            return (Math.Abs(a.X - b.X)
                + Math.Abs(a.Y - b.Y)
                + Math.Abs(a.Z - b.Z)) / 2;
        }

        private Vector3 ConvertToCube(Point point)
        {
            var x = point.Y;
            var z = point.X - (point.Y - (point.Y & 1)) / 2;
            var y = -x - z;
            return new Vector3(x, y, z);
        }

        private List<Point> GenerateVoronoiPoints(List<TiledTile> tiles, int count)
        {
            var points = new List<Point>();

            for(var i = 0; i < count; i++)
            {
                var randomTile = tiles.PickRandom();
                points.Add(new Point(randomTile.x, randomTile.y));
            }

            return points;
        }
    }
}
