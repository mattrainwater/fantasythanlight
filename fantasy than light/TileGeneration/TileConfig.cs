﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FantasyThanLight.TileGeneration
{
    public class TileConfig
    {
        public TileConfig()
        {
            Terrains = new List<Terrain>();
        }

        public int OceanTileId { get; set; }
        public int ShallowOceanTileId { get; set; }
        public int MountainTileId { get; set; }
        public List<Terrain> Terrains { get; set; }
    }
}
